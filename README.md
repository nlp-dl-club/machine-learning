# Machine Learning

This is a Machine Learning repo. All the resources are cummulative collection of the work done by **NLP-DL Club**, **`Department of AI, VJIT`**


##  **Copyright (C) 2020-2021 by the Free Software Foundation, Inc**.


 _**This Repo is part of NLP-DL Club, Vidya Jyothi Institute of Technology**._

 _**Machine Learning**_ is free open source repository for self-learning: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free
 Software Foundation, either version 3 of the License, or (at your option)
 any later version.

 This repo is public in the hope that it will be useful, but **WITHOUT
 ANY WARRANTY**; without even the implied warranty of **MERCHANTABILITY** or
 **FITNESS FOR A PARTICULAR PURPOSE**.  See the **GNU General Public License** for
 more details.

 You should have received a copy of the **GNU General Public License** along with
 this repository.  If not, see <https://www.gnu.org/licenses/>.
